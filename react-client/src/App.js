import React, { Component } from 'react';
import { BrowserRouter as Router} from 'react-router-dom';
import Users from './users/Users';
import Auth from './auth/Auth';
import './App.css';

class App extends Component {

  render() {
    return (
      <Router>
        <div className="App">
          <Auth />
          <Users />
        </div>
      </Router>
    );
  }
}

export default App;
