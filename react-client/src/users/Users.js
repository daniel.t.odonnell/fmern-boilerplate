import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import axios from 'axios';
import UserList from './UserList';
import AddUser from './AddUser';

class Users extends Component {

  render() {
    return (
        <Switch>
            <Route path="/users" exact={true} render={(props) => <UserList {...props} didMount={this.UserListDidMount} />} />
            <Route path="/users/add-user" render={(props) => <AddUser {...props} onSubmit={this.AddUserOnSubmit} />} />
        </Switch>
    );
  }

  UserListDidMount(context) {
    axios.get('/users')
      .then(res => context.setState({ users : res.data }))
      .catch(error => console.log(error));
  }

  AddUserOnSubmit(context, user) {
    axios.post('/users', user)
      .then(res => context.props.history.push('/users'))
      .catch(error => console.log(error));
  }

}

export default Users;
