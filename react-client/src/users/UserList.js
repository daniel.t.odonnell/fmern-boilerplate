import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class UserList extends Component {
  state = {users: []}
  
  componentDidMount() {
    this.props.didMount(this)
  }
    
  render() {
    // https://goshakkk.name/controlled-vs-uncontrolled-inputs-react/
    return (
      <div>
        <h1>Users</h1>
        {this.state.users.map(user =>
          <div key={user.id}>Id: {user.id} - Handle: {user.handle}</div>
        )}        
        <p><Link to="/users/add-user">Add New User</Link></p>
      </div>
    )
  }
}

UserList.propTypes = {
  didMount: PropTypes.func.isRequired
};

export default UserList;
