import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      handle: null,
      validation: {
        id_ok    : true,
        handle_ok: true
      }
    };

    // makes "this" and "this.state" available in events
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(evt) {
    let target = evt.target;
    this.setState({ [target.name]: target.value });
  }

  handleSubmit(evt) {

    // validation
    let valid = true;
    let newState = {...this.state};

    if (!this.state.id || this.state.id.length === 0) {
      valid = false;
      newState = { validation : {...newState.validation, ...{ id_ok : false }}}
    }
    else {
      newState = { validation : {...newState.validation, ...{ id_ok : true }}}
    }
    if (!this.state.handle || this.state.handle.length === 0) {
      valid = false;
      newState = { validation : {...newState.validation, ...{ handle_ok : false }}}
    }
    else {
      newState = { validation : {...newState.validation, ...{ handle_ok : true }}}
    }

    // setState is async and batched
    this.setState(newState);
        
    if (valid) {      
      this.props.onSubmit(this, this.state);
    }

    evt.preventDefault();
  }
    
  render() {
    return (
      <form  onSubmit={this.handleSubmit}>
        <p><label>Id:<input 
          type="text" 
          name="id"
          value={this.state.id || ""}
          onChange={this.handleInputChange} />
          { !this.state.validation.id_ok ? <span>* required</span> : null } 
        </label></p>
        <p><label>Handle:<input 
          type="text" 
          name="handle" 
          value={this.state.handle || ""}
          onChange={this.handleInputChange} />
          { !this.state.validation.handle_ok ? <span>* required</span> : null } 
        </label></p>
        <input type="submit" value="Submit" />
      </form>
    )
  }
}

AddUser.propTypes = {
  onSubmit: PropTypes.func.isRequired
};


export default AddUser;
