import firebase from 'firebase';

var config = {
  apiKey: "AIzaSyC49M8GBgaOI4L8IvU6i9jjoOIKlvEFip0",
  authDomain: "boilingplastic.firebaseapp.com",
  databaseURL: "https://boilingplastic.firebaseio.com",
  projectId: "boilingplastic",
  storageBucket: "boilingplastic.appspot.com",
  messagingSenderId: "102144305269"
};

var fire = firebase.initializeApp(config);

export default fire;