import React from 'react';
import { BrowserRouter as Router} from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
//import { console } from '@storybook/addon-console';

import MockData from './MockData';
import UserList from './../users/UserList';
import AddUserControlled from './../users/AddUserControlled';

storiesOf('Users', module)
  /////////////////////////////////////////
  .add('List of Users', () => {
    const data = { users : [
      {id: "test1", handle: "HANDLE1"},
      {id: "test2", handle: "HANDLE2"},
      {id: "test3", handle: "HANDLE3"}
    ]};
    const callback = (context) => {
      context.setState(data);
    };
    return renderUserList(callback);
  })
  /////////////////////////////////////////
  .add('Add Controlled User', () => {
    /*const callback = (context, user) => {
      alert(JSON.stringify(user));
    };*/
    const callback = (context, user) => {
      console.log("onSubmit", context, user);
      return false;
    }

    return renderAddUserControlled(callback);
  });


function renderUserList(didMount) {
  return (
    <Router><UserList didMount={didMount} /></Router>
  );
}
function renderAddUserControlled(onSubmit) {
  return (
    <AddUserControlled onSubmit={onSubmit} />
  );
}
