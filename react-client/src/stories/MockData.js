import React, { Component } from 'react';

class MockData extends Component {
  UserList() {
    return [
      {id: "test1", handle: "HANDLE1"},
      {id: "test2", handle: "HANDLE2"},
      {id: "test3", handle: "HANDLE3"}
    ];
  }
}

export default MockData;