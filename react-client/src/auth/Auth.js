import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import axios from 'axios';
import DoAuth from './DoAuth';
import fire from './../fire';

class Auth extends Component {

  componentWillMount() {
    fire.auth().onAuthStateChanged(this.checkAuth);      
  }

  checkAuth(user) {
    if (user) {
      alert(JSON.stringify(user));
    } else {
      // No user is signed in.
      if (window.location.pathname !== "/") {
        alert("Must login");
        window.location.href = "/";
      }
    }
  }

  render() {
    return (
        <Switch>
            <Route path="/" exact={true} render={(props) => <DoAuth {...props} />} />
        </Switch>
    );
  }

}

export default Auth;
