import React, { Component } from 'react';
import fire from './../fire';


class DoAuth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      password: null,
      validation: {
        id_ok    : true,
        password_ok: true
      }
    };

    // makes "this" and "this.state" available in events
    this.passwordInputChange = this.passwordInputChange.bind(this);
    this.passwordSubmit = this.passwordSubmit.bind(this);
  }

  passwordInputChange(evt) {
    let target = evt.target;
    this.setState({ [target.name]: target.value });
  }

  passwordSubmit(evt) {

    // validation
    let valid = true;
    let newState = {...this.state};

    if (!this.state.id || this.state.id.length === 0) {
      valid = false;
      newState = { validation : {...newState.validation, ...{ id_ok : false }}}
    }
    else {
      newState = { validation : {...newState.validation, ...{ id_ok : true }}}
    }
    if (!this.state.password || this.state.password.length === 0) {
      valid = false;
      newState = { validation : {...newState.validation, ...{ password_ok : false }}}
    }
    else {
      newState = { validation : {...newState.validation, ...{ password_ok : true }}}
    }

    // setState is async and batched
    this.setState(newState);
        
    if (valid) {      
      fire.auth().signInWithEmailAndPassword(this.state.id, this.state.password)
      .then(
        res => alert("ok!")
      )
      .catch(
        error => alert(`${error.code}: ${error.message}`)
      );
    }

    evt.preventDefault();
  }
    
  render() {
    return (
      <form  onSubmit={this.passwordSubmit}>
        <p><label>Email:<input 
          type="text" 
          name="id"
          value={this.state.id || ""}
          onChange={this.passwordInputChange} />
          { !this.state.validation.id_ok ? <span>* required</span> : null } 
        </label></p>
        <p><label>Password:<input 
          type="text" 
          name="password" 
          value={this.state.password || ""}
          onChange={this.passwordInputChange} />
          { !this.state.validation.password_ok ? <span>* required</span> : null } 
        </label></p>
        <input type="submit" value="Submit" />
      </form>
    )
  }
}

export default DoAuth;
