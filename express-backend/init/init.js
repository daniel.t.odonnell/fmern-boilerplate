const schemas = require('./../db/schemas');

const InitMongo = function() {
  console.log(">>>>>>>>>>>>>INIT<<<<<<<<<<<<<<<");

  let User = schemas.User;

  User.count({}, function(err, data) {
    if (data === 0) {
      console.log("INIT: Initializing dummy user data");
      let dan = new User();
      dan.id = "dan";
      dan.handle = "Viper";  
      dan.save();  
      let elliott = new User();
      elliott.id = "elliot";
      elliott.handle = "Stabler";  
      elliott.save();      
    }
    else {
      console.log(`INIT: We have ${data} user records already`);
    }
  });
}

module.exports = InitMongo;