const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/express-backend');
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

mongoose.connection.once('open', function() {});  

module.exports = mongoose;